import express from 'express'
import path from 'path'
import expressLayouts from 'express-ejs-layouts'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import session from 'express-session'
import flash from 'connect-flash'
import methodOverride from 'method-override'
import { setupRoutes, connectDatabase, config, setupAuth } from './config'
import { setupCurrentUser, setupFlashMessages } from './app/middlewares'
import * as appHelpers from './app/helpers/app'
function registerAppHelpers(app) {
  app.locals = { ...app.locals, ...appHelpers }
}
export function setupServer() {
  const app = express()
  const { port, host } = config
  app.set('view engine', 'ejs')
  app.set('views', path.join(__dirname, 'app/views'))
  app.set('layout', 'shared/layout')
  app.use(expressLayouts)
  app.use(morgan('tiny'))
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(methodOverride('_method'))
  app.use(
    session({
      secret: config.secret,
      resave: false,
      saveUninitialized: true
    })
  )
  app.use(flash())
  app.use(setupFlashMessages)
  registerAppHelpers(app)
  setupAuth(app)
  app.use(setupCurrentUser)
  setupRoutes(app)
  connectDatabase().then(() => {
    app.listen(port, host, () => console.log(`listening on port ${port}!`))
  })
}
