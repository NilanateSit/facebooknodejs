import { check } from 'express-validator/check'
import passport from 'passport'

import controller from './controller'

const TodoValidators = [
  check('title')
    .isLength({ min: 1, max: 50 })
    .withMessage('must contain 1 - 50 chars long'),
  check('desc')
    .isLength({ min: 1 })
    .withMessage('must be at least 1 char')
]

const authJwt = () => passport.authenticate('jwt', { session: false })

export function setup(router) {
  router
    .get('/:slug', controller.show)
    .get('/', controller.index)
    .post('/', [authJwt(), TodoValidators], controller.create)
    .patch('/:slug', [authJwt(), TodoValidators], controller.update)
    .delete('/:slug', controller.destroy)
}
