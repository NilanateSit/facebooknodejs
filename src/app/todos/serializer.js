import Serializer from '@common/serializer'

export default {
  ...Serializer,

  index(todos) {
    return todos.map(({ id, slug, title }) => ({ id, slug, title }))
  },

  create(todo) {
    return this.serializeTodo(todo)
  },

  update(todo) {
    return this.serializeTodo(todo)
  },

  show(todo) {
    return this.serializeTodo(todo)
  },

  serializeTodo(todo) {
    const { _id, __v, ...rest } = todo._doc

    return { id: _id, ...rest }
  }
}
