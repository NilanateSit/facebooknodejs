import mongoose from 'mongoose'

const Schema = mongoose.Schema
const todoSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  desc: {
    type: String,
    required: true
  },
  slug: {
    type: String,
    required: true,
    unique: true
  },
  userId: {
    type: Schema.Types.ObjectId,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  updated: {
    type: Date,
    default: Date.now
  }
})

todoSchema.methods.generateSlug = function() {
  this.slug = `${this.title.toLowerCase().replace(/ +/g, '-')}-${this.id}`
}

todoSchema.statics.findOneBySlug = function(slug) {
  return Todo.findOne({ slug })
}

const Todo = mongoose.model('Todo', todoSchema)

export default Todo
