import { check } from 'express-validator/check'

import { tasks as controller } from '@controllers'

const postValidation = [
  check('title')
    .isLength({ min: 1, max: 50 })
    .withMessage('must contain 1 - 50 chas long'),
  check('description')
    .isLength({ min: 1 })
    .withMessage('must be at least 1 char')
]

export default router => {
  router
    .get('/', controller.index)
    .get('/new', controller.build)
    .get('/:id', controller.show)
    .post('/', postValidation, controller.create)
    .patch('/:id', controller.update)
}
