import { check } from 'express-validator/check'

import { posts as controller } from '@controllers'

const postValidation = [
  check('title')
    .isLength({ min: 1, max: 50 })
    .withMessage('must contain 1 - 50 chas long'),
  check('content')
    .isLength({ min: 1 })
    .withMessage('must be at least 1 char')
]

export default router => {
  router
    .get('/', controller.index)
    .get('/new', controller.build)
    .get('/:slug', controller.show)
    .get('/:slug/edit', controller.edit)
    .post('/', postValidation, controller.create)
    .patch('/:slug', postValidation, controller.update)
    .delete('/:slug', controller.destroy)
}
