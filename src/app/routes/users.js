import passport from 'passport'
import { users as controller } from '@controllers'
export default router => {
  router
    .post(
      '/',
      passport.authenticate('local-signup', {
        successRedirect: '/',
        failureRedirect: '/users/signup',
        failureFlash: true
      })
    )
    .post(
      '/signin',
      passport.authenticate('local-login', {
        successRedirect: '/',
        failureRedirect: '/users/signin',
        failureFlash: true
      })
    )
    .get('/signup', controller.signup)
    .get('/signin', controller.signin)
    .get('/signout', controller.signout)
}
