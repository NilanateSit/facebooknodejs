const signup = (_, res) => {
  res.render('users/signup')
}
const signin = (_, res) => {
  res.render('users/signin')
}
const signout = (req, res) => {
  req.logout()
  res.redirect('/')
}
export default { signup, signin, signout }
