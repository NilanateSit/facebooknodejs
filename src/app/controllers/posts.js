import { validationResult } from 'express-validator/check'

import { Post } from '@models'

const index = (_, res) => {
  Post.find().then(posts => {
    res.render('posts/index', { posts })
  })
}

const create = (req, res) => {
  const errors = validationResult(req)

  if (!errors.isEmpty()) {
    return res.render('posts/build', {
      errors: errors.mapped(),
      errorMessage: 'Invalid Input!'
    })
  }

  const { title, content } = req.body
  const post = new Post({ title, content })

  post.generateSlug()

  post.save().then(() => {
    req.flash('success', 'The post has been created.')
    res.redirect('/posts')
  })
}

const build = (_, res) => {
  res.render('posts/build', { errors: {} })
}

const show = (req, res) => {
  Post.findOneBySlug(req.params.slug).then(post =>
    res.render('posts/show', { post })
  )
}

const edit = (req, res) => {
  Post.findOneBySlug(req.params.slug).then(post =>
    res.render('posts/edit', { post })
  )
}

const update = (req, res) => {
  Post.findOneBySlug(req.params.slug)
    .then(post => {
      const { title, content } = req.body

      post.title = title
      post.content = content

      return post.save()
    })
    .then(post => {
      res.render('posts/show', {
        post,
        successMessage: 'The post has been updated.'
      })
    })
}

const destroy = (req, res) => {
  Post.deleteOne({ slug: req.params.slug }).then(() => {
    req.flash('success', 'The post has been deleted.')
    res.redirect('/posts')
  })
}

export default { index, create, build, show, edit, update, destroy }
