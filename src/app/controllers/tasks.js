import { validationResult } from 'express-validator/check'

import { Task } from '@models'

const index = (_, res) => {
  Task.find().then(tasks => {
    res.render('tasks/index', { tasks })
  })
}

const create = (req, res) => {
  const errors = validationResult(req)

  if (!errors.isEmpty()) {
    return res.render('tasks/build', {
      errors: errors.mapped(),
      errorMessage: 'Invalid Input!'
    })
  }

  const { title, description } = req.body
  const task = new Task({ title, description })

  task.save().then(() => {
    req.flash('success', 'The task has been created.')
    res.redirect('/tasks')
  })
}

const build = (_, res) => {
  res.render('tasks/build', { errors: {} })
}

const show = (req, res) => {
  Task.findOne({ _id: req.params.id }).then(task =>
    res.render('tasks/show', { task })
  )
}

// const edit = (req, res) => {
//   Post.findOneBySlug(req.params.slug).then(post =>
//     res.render('posts/edit', { post })
//   )
// }

const update = (req, res) => {
  Task.findOne({ _id: req.params.id })
    .then(task => {
      const { title, description, status } = req.body

      task.title = title
      task.description = description
      task.status = status

      return task.save()
    })
    .then(task => {
      res.render('tasks/show', {
        task,
        successMessage: 'The task has been updated.'
      })
    })
}

// const destroy = (req, res) => {
//   Post.deleteOne({ slug: req.params.slug }).then(() => {
//     req.flash('success', 'The post has been deleted.')
//     res.redirect('/posts')
//   })
// }

export default { index, build, create, show, update }
