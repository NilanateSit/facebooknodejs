export default (req, res, next) => {
  res.locals.errorMessage = req.flash('error')[0]
  res.locals.successMessage = req.flash('success')[0]

  next()
}
