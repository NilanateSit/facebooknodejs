import mongoose from 'mongoose'

const Schema = mongoose.Schema
const postSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  slug: {
    type: String,
    required: true,
    unique: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  updated: {
    type: Date,
    default: Date.now
  }
})

postSchema.methods.generateSlug = function() {
  this.slug = `${this.title.toLowerCase().replace(/ +/g, '-')}-${this.id}`
}

postSchema.statics.findOneBySlug = function(slug) {
  return Post.findOne({ slug })
}

const Post = mongoose.model('Post', postSchema)

export default Post
