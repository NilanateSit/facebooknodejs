import mongoose from 'mongoose'

const Schema = mongoose.Schema
const taskSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  status: {
    type: Boolean,
    required: true,
    default: false
  },
  created_by: {
    type: Schema.Types.ObjectId,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  updated: {
    type: Date,
    default: Date.now
  }
})

const Task = mongoose.model('Task', taskSchema)

export default Task
