function flashMessage(type, message) {
  return message
    ? `
    <div class="alert alert-${type}">
      ${message}
      <button type="button" class='close' data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  `
    : ''
}

function flashMessages(successMessage, errorMessage) {
  const mappingMessages = {
    danger: errorMessage,
    success: successMessage
  }

  return Object.keys(mappingMessages)
    .map(type => flashMessage(type, mappingMessages[type]))
    .join('')
}

function generateLabel(name = '') {
  return `<label for="${name}">${name[0].toUpperCase() + name.slice(1)}</label>`
}

function generateInput(name, value = '', options = {}) {
  const { inputType = 'input', type = 'text', errors = {} } = options
  const className = `form-control ${errors[name] ? 'is-invalid' : ''}`
  const placeholder = `Enter ${name}`
  const props = `
    id="${name}"
    name="${name}"
    placeholder="${placeholder}"
    class="${className}"
  `

  switch (inputType) {
    case 'input':
      return `
        <input ${props} type="${type}" value="${value}" />
      `
    case 'textarea':
      return `<textarea ${props}>${value}</textarea>`
  }
}

function generateErrorMessages(name, errors = {}) {
  return `<div class="invalid-feedback">${errors[name] &&
    errors[name].msg}</div>`
}

function formGroup(name, options = {}) {
  const { value, ...rest } = options
  let result = `
    <div class="form-group">
      ${generateLabel(name)}
      ${generateInput(name, value, rest)}
      ${generateErrorMessages(name, options.errors)}
    </div>
  `

  return result
}

export { flashMessages, formGroup }
