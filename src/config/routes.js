import fs from 'fs'
import express from 'express'
import { promisify } from 'util'
function setupFeatureRoutes(app) {
  const readdir = promisify(fs.readdir)
  const routesPath = `${__dirname}/../app/routes`
  readdir(routesPath).then(files => {
    files.filter(file => file !== 'index.js').forEach(file => {
      import(`${routesPath}/${file}`).then(routes => {
        const router = express.Router()
        const routeName = file.split('.')[0]
        routes.default(router)
        app.use(`/${routeName}`, router)
      })
    })
  })
}
function setupPageRoutes(app) {
  app.get('/', (req, res) => res.render('pages/home'))
}
export default function setupRoutes(app) {
  setupFeatureRoutes(app)
  setupPageRoutes(app)
}
