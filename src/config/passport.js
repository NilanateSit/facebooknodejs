import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'
import { User } from '@models'
export default app => {
  app.use(passport.initialize())
  app.use(passport.session())
  passport.serializeUser((user, done) => {
    done(null, user.id)
  })
  passport.deserializeUser((id, done) => {
    User.findById(id)
      .then(user => done(null, user))
      .catch(err => done(err))
  })
  passport.use(
    'local-signup',
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
      },
      (req, email, password, done) => {
        User.findOne({ 'local.email': email })
          .then(user => {
            if (user) {
              return done(
                null,
                false,
                req.flash('error', 'That email is already taken.')
              )
            }
            const newUser = new User()
            newUser.local.email = email
            newUser.local.password = newUser.encryptPassword(password)
            newUser.save().then(user => done(null, user))
          })
          .catch(err => done(err))
      }
    )
  )
  passport.use(
    'local-login',
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
      },
      (req, email, password, done) => {
        User.findOne({ 'local.email': email })
          .then(user => {
            const isInvalid = !(user && user.validPassword(password))
            if (isInvalid) {
              return done(
                null,
                false,
                req.flash('error', 'Oops! Invalid Credentials.')
              )
            }
            return done(null, user)
          })
          .catch(err => done(err))
      }
    )
  )
}
